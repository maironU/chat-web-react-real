import React from 'react'
import Background from '../components/Background'
import Register from '../components/Register'
import Preloader from '../components/Preloader'


const PageRegister = () => {
    return(
        <>
            <Preloader />
            <Background />
            <Register />
        </>
    )
}

export default PageRegister