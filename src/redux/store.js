import { createStore, combineReducers, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunk from 'redux-thunk'
import reducerRegister from './reducers/reducerRegister'
import reducerIsToken from './reducers/reducerIsToken'
import reducerContact from './reducers/reducerContact'
import reducerSendMessageToUser from './reducers/reducerSendMessageToUser'
import reducerSaveMessage from './reducers/reducerSaveMessage'
import reducerMessageServer from './reducers/reducerMessageServer'
import reducerNewsMessages from './reducers/reducerNewsMessages'

const reducers = combineReducers ({
    reducerRegister,
    reducerIsToken, 
    reducerContact,
    reducerSendMessageToUser,
    reducerSaveMessage,
    reducerMessageServer,
    reducerNewsMessages
})

const store = createStore(
    reducers,
    composeWithDevTools(
        applyMiddleware(thunk)
    )
)

export default store