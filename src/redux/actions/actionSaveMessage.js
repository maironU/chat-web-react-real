import axios from 'axios'

export const SUCCESS_MESSAGE = "SUCCESS_MESSAGE"

export const Success = (message) => {
    return {
        type: SUCCESS_MESSAGE,
        message
    }
}

const SaveMessage = (message, idFrom, idTo) => {
    return (dispatch) =>{ 
        axios.post("http://localhost:5000/apiMessage/nuevo/mensaje",{
            idFrom,
            idTo,
            message
        }).then(response => {
            dispatch(Success([response.data]))
        })
    }
}

export default SaveMessage