import axios from 'axios'

export const LOADING_CONTACTS = "LOADING_CONTACTS"
export const SUCCESS_CONTACTS = "SUCCESS_CONTACTS"

export const Loading = () => {
    return {
        type: LOADING_CONTACTS
    }
}

export const Success = (contactos) => {
    return {
        type: SUCCESS_CONTACTS,
        payload: contactos
    }
}

const FetchContacts = (id) => {
    return (dispatch => {
        const token = localStorage.getItem("token");
        dispatch(Loading())
        axios.get(`http://localhost:5000/apiContact/contactoPorId/${id}`,{
            headers: {'Authorization': `Bearer ${token}`}
        }).then(response => {
            console.log( "succes", response.data)
            dispatch(Success([response.data]))
        })
    })
}   

export default FetchContacts