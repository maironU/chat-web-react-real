
export const NEWS_MESSAGES = "NEWS_MESSAGES"


export const saveNewsMessagesNew = (array) => {
    return {
        type: NEWS_MESSAGES,
        payload: array
    }
}   

export const numsMessagesNews = (messages) => {

    return (dispatch) => {
        const  from = messages.idFrom._id;
        const arrayMessagesNews = []

        const data = {
            from,
            num: 1
        }
        arrayMessagesNews.push(data)

        let numMessagesNew = localStorage.getItem("numsMessages")
        
        if(numMessagesNew === null){

            localStorage.setItem("numsMessages", JSON.stringify(arrayMessagesNews))
            dispatch(saveNewsMessagesNew(arrayMessagesNews))
        }else{

           let array = JSON.parse(localStorage.getItem("numsMessages"))

            for(var i = 0; i < array.length; i++){
                if(array[i].from === from){
                    array[i].num = array[i].num + 1;
                    localStorage.setItem("numsMessages", JSON.stringify(array))
                    dispatch(saveNewsMessagesNew(array))
                }else{
                    array.concat(data)
                    localStorage.setItem("numsMessages", JSON.stringify(array))
                    dispatch(saveNewsMessagesNew(array))
                }
            }
        }
    }
}

export const deleteMessagesNews = (messages) => {
    return (dispatch) => {
        const arrayMessages = JSON.parse(localStorage.getItem("numsMessages"))
        const from = messages.idContact._id

        if(arrayMessages != null){
            for(var i = 0; i < arrayMessages.length; i++){
                if(arrayMessages[i].from === from){
                    arrayMessages.splice(i, 1);
                    localStorage.setItem("numsMessages", JSON.stringify(arrayMessages))

                    if(arrayMessages.length === 0){
                        localStorage.removeItem("numsMessages")
                    }
                    dispatch(saveNewsMessagesNew(arrayMessages))
                }else{
                    dispatch(saveNewsMessagesNew(arrayMessages))
                }
            }
        }
    }
}   