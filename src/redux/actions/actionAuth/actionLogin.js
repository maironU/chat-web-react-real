import axios from 'axios'
import history from '../../../history';
import SweetAlert from 'sweetalert2'
import jwt from 'jsonwebtoken'
import socketIoClient from 'socket.io-client'


const FetchLogin = (data, ref) => {

    var socket = socketIoClient("http://192.168.1.3:5000");
    socket.emit("hola", "hola");

    return (dispatch) => {
        axios.post("http://localhost:5000/api/login", data)
             .then(response => {
                if(response.data.length === 0){
                    var socket = socketIoClient("http://192.168.1.3:5000");

                    socket.emit("sweet", "sweet");

                    SweetAlert.fire({
                        title: "Error",
                        icon: "error",
                        text:"Usuario o contraseña incorrectas",
                    })
                    .then(confirm => {
                        if(confirm){
                            const pass = ref.current;
                            pass.value = ""
                        }
                    })
                }else{
                    var socket = socketIoClient("http://192.168.1.3:5000");

                    socket.emit("holadenuevo", "hola de nuevo");

                    localStorage.setItem("token", response.data.token)
                    history.push("/")   
                    const user = jwt.decode(response.data.token)
                }   
                
             })
    }
}

export default FetchLogin

