export const usersResults = state => {
    return state.reducerIsToken.user[0];
}

export const contactsResults = state => {
    return state.reducerContact.contacts
}

export const infoMyContactResults = state => {
    return state.reducerSendMessageToUser.contact
}

export const messagesFromMyContact = state => {
    return state.reducerMessageServer.message
}

export const arrayNewsMessages = state => {
    return state.reducerNewsMessages.newsMessages
}