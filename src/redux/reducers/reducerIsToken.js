import { isToken } from '../../authService/verifyToken'

const initialState = {
    user: ""
}

const reducerIsToken = (state = initialState, action) => {
    switch(action.type){
        case isToken:
            return {
                ...state,
                user: action.payload
            }
        default:
            return state
    }
}

export default reducerIsToken