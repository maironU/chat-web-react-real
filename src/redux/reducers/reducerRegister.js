import { LOADING_REGISTER, SUCCESS_REGISTER, ERROR_REGISTER } from '../actions/actionAuth/actionRegister'

const initialState = {
    loading: false,
    messageCreate: "",
    error: ""
}

const reducerRegister = (state = initialState, action) => {
    switch(action.type){
        case LOADING_REGISTER:
            return {
                ...state,
                loading: true
            }
        case SUCCESS_REGISTER:
            return {
                ...state,
                loading: false,
                messageCreate: action.payload,
                error: ""
            }
        case ERROR_REGISTER: 
            return {
                ...state,
                error: action.payload,
                loading: false,
                messageCreate: ""
            }
        default:
            return state
    }
}

export default reducerRegister