import { NEWS_MESSAGES } from '../actions/actionNumsMessagesNews'

const initialState = {
    newsMessages : []
}

const reducerNewsMessages = (state = initialState, action) => {
    switch(action.type){
        case NEWS_MESSAGES:
            return {
                ...state,
                newsMessages: action.payload
            }
        default: 
            return state;
    }
}

export default reducerNewsMessages