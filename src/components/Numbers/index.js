import React, {useEffect, useState} from 'react'
import { ContainerNumbers, Number, Logo, ContainerInfo, ContainerNameAndDate, Name, Date,
    ContainerNumberAndMessages,Phone, ContainerNumMessagesNew, NumMessagesNew } from './styled'
import FetchContacts from '../../redux/actions/actionContacts'
import { useSelector, useDispatch } from 'react-redux'
import actionSendMessageToUser from '../../redux/actions/actionSendMessageToUser'
import { usersResults, contactsResults, infoMyContactResults } from '../../redux/selectors'
import { arrayNewsMessages } from '../../redux/selectors'
import {MessageServer} from '../../redux/actions/actionMessageServer'
import { numsMessagesNews } from '../../redux/actions/actionNumsMessagesNews'
import socketIoClient from 'socket.io-client'

const Numbers = () => {

    const dispatch = useDispatch()
    const data = useSelector(state => usersResults(state))
    const contacts = useSelector(state => contactsResults(state))
    const newMessages = useSelector(state => arrayNewsMessages(state))
    const [endpoint, setEndiPoint] = useState("http://192.168.1.3:5000");
    const ContentMessageNoRender = useSelector(state => infoMyContactResults(state))

    useEffect(() => {
        console.log("me renderice")
        dispatch(FetchContacts(data._id))

        var socket = socketIoClient(endpoint);
        socket.emit("id", data._id);

        socket.on("messages", message => { 
            const audioRecibo = document.getElementById("audioRecibo")
            audioRecibo.play()
            dispatch(MessageServer(message))
            dispatch(numsMessagesNews(message))
        })

    }, [])

    const SendMessageToUser = (contact) => {
        dispatch(actionSendMessageToUser(contact))
    }


    return(
        <>
        {contacts.length > 0 &&
            <>
                {contacts.map(function(arraycontact, key){
                    return(
                        <ContainerNumbers key = {key}>
                            <>
                            {arraycontact.map(function(contact, key){
                                return(
                                    <Number key= {key} onClick = {() => SendMessageToUser(contact)}>
                                        <Logo src = "https://scontent-yyz1-1.cdninstagram.com/v/t51.2885-15/e35/77331947_2893011124063503_5483528057090871184_n.jpg?_nc_ht=scontent-yyz1-1.cdninstagram.com&_nc_cat=103&_nc_ohc=9nBhZv79z4YAX8PXw_k&oh=f9c3def29676166ead2e414022bb18ee&oe=5EC2217A"/>
                                        <ContainerInfo>
                                            <ContainerNameAndDate>
                                                <Name>{contact.idContact.name}</Name>
                                                <Date>12:54 p.m</Date>
                                            </ContainerNameAndDate>
                                            <audio id = "audioRecibo" src = "http://192.168.1.3:4000/src/sound/recibo.mp3"></audio>
                                            <ContainerNumberAndMessages>
                                                <Phone>+57 {contact.idContact.number}</Phone>
                                                {newMessages.map(function(numMessages, key){
                                                    return(
                                                        <ContainerNumMessagesNew key ={key}>
                                                            {numMessages.from === contact.idContact._id  && ContentMessageNoRender.length === 0 &&
                                                                <NumMessagesNew>{numMessages.num}</NumMessagesNew>           
                                                            }   
                                                        </ContainerNumMessagesNew>
                                                    )
                                                })

                                                }
                                            </ContainerNumberAndMessages>
                                        </ContainerInfo>
                                    </Number>
                                )
                            })  
                            }
                            </>
                    </ContainerNumbers>
                    )
                })

                }
            </>
        }

        </>
    )
}

export default Numbers