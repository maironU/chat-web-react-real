import styled from 'styled-components'

export const ContainerContentMessages = styled.div`
    width: 100%;
    height: calc(100% - 125px);
    background-image: ${props => props.background};
    display: flex;
    flex-direction: column;
    position: relative;
    margin-top: 60px;
    overflow-y: scroll;
    background: ${props => props.background};

    ::-webkit-scrollbar {
        display: none;
    }
`

export const ContainerMessage = styled.div`
    width: fit-content;
    min-width: 15px;
    border-radius: 10px;
    background: ${props => props.color};
    display: flex;
    flex-flow: column wrap;
    padding: 0 10px 0 10px;
    position: relative;
    min-height: 45px;
    margin: 0 30px 15px ${props => props.margin_left};
`

export const Message = styled.p`
    width: fit-content;
    margin: 0;
    word-break: break-all;
`

export const Hour = styled.span`
    width: fit-content;
    position: absolute;
    right: 10px;
    bottom: 0;
    font-size: 10px;
    font-weight: 400;
`

// Si no hay mensajes

export const ContainerNoMessages = styled.div`
    width: 50%;
    margin: 0 auto;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    border-bottom: 1px solid #ddd;
    height: calc(100% - 125px);
`

export const Logo = styled.img`
    width: 200px;
    height: 200px;
    border-radius: 50%;
`
export const Span = styled.span`
    font-size: 25px;
    font-weight: 600;
    margin-top: 10px;
    
`