import React, { useEffect, useState } from 'react'
import { ContainerContentMessages, ContainerMessage, Message, Hour, Logo, ContainerNoMessages,
    Span } from './styled'
import { useSelector, useDispatch } from 'react-redux'
import {FetchMessages, MessageServer} from '../../redux/actions/actionMessageServer'
import { usersResults, infoMyContactResults, messagesFromMyContact } from '../../redux/selectors'
import { deleteMessagesNews } from '../../redux/actions/actionNumsMessagesNews'
import socketIoClient from 'socket.io-client'

const ContentMessages = () => {

    const messages = useSelector(state => messagesFromMyContact(state))
    const from = useSelector(state => usersResults(state))
    const to = useSelector(state => infoMyContactResults(state))
    const [Background, setBackground] = useState("")
    const [endpoint, setEndiPoint] = useState("http://192.168.1.3:5000");

    const dispatch = useDispatch()

    if(messages.length > 0){
        console.log("primero como" ,messages)
        
    }

    useEffect(() => {
        if(typeof to.idContact != 'undefined'){
            dispatch(FetchMessages(from._id, to.idContact._id))    
            dispatch(deleteMessagesNews(to))
        }

        if (to.length != 0){
            setBackground('url("https://user-images.githubusercontent.com/15075759/28719144-86dc0f70-73b1-11e7-911d-60d70fcded21.png")')
        }else{
            setBackground("white")
        }

    },[to])

    useEffect(() => {
        var conten = document.getElementById("content")
        conten.scrollTop = conten.offsetHeight + 10000000000
    },[messages])

    return (   
        <ContainerContentMessages id="content" background = {Background}>
            {messages.length > 0 && to.length != 0 ?
                <>
                    {messages.map(function(message, key) {
                        return(
                            <>
                                {(message.idFrom._id === to.idContact._id || message.idTo._id === to.idContact._id ) && 
                                    <ContainerMessage key = {key} color={from._id === message.idFrom._id? "#C9FFBF":"#F8FEF7"}
                                    margin_left={from._id === message.idFrom._id? "auto":"50px"}>
                                        <Message>{message.message}</Message>
                                        <Hour></Hour>
                                    </ContainerMessage>
                                }
                            </>
                        )
                    })

                    }
                </>
                :
                <ContainerNoMessages>
                    <Logo src = "https://wtcradio.net/wp-content/uploads/2018/09/noticia-whatsapp-truco-viral-ocultar-aplicacion-mensajeria.png"/>
                    <Span>Empieza un nuevo chat</Span>
                </ContainerNoMessages>
            }
        </ContainerContentMessages>
    )
}

export default ContentMessages