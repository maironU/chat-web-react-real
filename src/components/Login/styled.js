import styled from 'styled-components'
import { Link as BrowserLink } from 'react-router-dom'

export const ContainerLogin = styled.div`
    width: 300px;
    padding: 20px 0 20px 0;
    margin: 50px auto;
    text-align: center;
    border: 1px solid #dbdbdb;
    border-radius: 10px;
`
export const ContainerForm = styled.div`
    width: 80%;
    margin: 0 auto;
    display: flex;
    flex-direction: column;

    input {
        padding: 10px;
        margin-bottom: 10px;
        border-radius: 5px; 
        border: 1px solid #dbdbdb;

        :focus {
            outline-color: green;
        }
    }

    input[type=submit] {
        width: 100%;
        background-color: rgba(0,149,246,.3);
        cursor: pointer;
        color: white;

        :hover {
            background-color: rgba(0,149,246,.6);
        }
    }
`
export const Link = styled(BrowserLink)`
    text-decoration: none;
    color: rgb(0,149,246);
`
export const Message = styled.div`
    width: 100%;
    margin-bottom: 30px;
    color: green;
`