import React from 'react'
import { useSelector } from 'react-redux'
import { ContainerSearchContact, Input } from './styled'

const SearchContact = () => {

    return(
        <ContainerSearchContact>
            <Input type ="text" placeholder = "Buscar o empezar chat nuevo"/>
        </ContainerSearchContact>
    )
}

export default SearchContact