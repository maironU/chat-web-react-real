import styled from 'styled-components'

export const Container = styled.div`
    width: 32vw;
    height: 100vh;
    box-sizing: border-box;
    border-right: 1px solid #ddd;
    position: relative;
`

