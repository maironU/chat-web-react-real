import styled from 'styled-components'

export const ContainerInputMessage = styled.div`
    width: 70vw;
    background-color: #EEEEEE;
    padding: 10px;
    display: flex;
    justify-content: space-around;
    position: fixed;
    bottom: 0;
`
export const Input = styled.input`
    width: 60%;
    border: none;
    padding: 15px;
    border-radius: 20px;

    :focus {
        outline: none;
    }
`
export const Button = styled.button`
    border: none;
    background: #79FE5F;
    border-radius: 20px;
    color: white;
    cursor: pointer;
`