import styled from 'styled-components'

export const ContainerHeaderChat = styled.div`
    width: 70%;
    background : #F9F9F9;
    padding: 10px;
    display: flex;
    align-items: center;
    border-bottom: 0.1px solid #ddd;
    z-index: 400;
    position: fixed;
    left: 30%;
`
export const Logo = styled.img`
    width: 40px;
    height: 40px;
    border-radius: 50%;
    margin-right: 10px;
`