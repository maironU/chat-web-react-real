import React from 'react'
import { useSelector } from 'react-redux'
import { ContainerHeaderProfile, LogoProfile } from './styled'
import { usersResults } from '../../redux/selectors'

const HeaderProfile = () => {

    const data = useSelector(state => usersResults(state))

    return(
        <ContainerHeaderProfile>
            <LogoProfile src="https://www.kindpng.com/picc/m/128-1282088_i-g-profile-icon-vector-png-transparent-png.png"/>
            <span>{data.name}</span>
        </ContainerHeaderProfile>
    )
}

export default HeaderProfile